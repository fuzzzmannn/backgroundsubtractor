import os
import numpy as np
import cv2
import sys

def load_folder(path, mode, limit=-1):
    imgs = []
    names = os.listdir(path)
    if "Thumbs.db" in names:
        names.remove("Thumbs.db")

    for i, name in enumerate(names):
        if limit >= 0 and i >= limit:
            break
        img = cv2.imread(os.path.join(path, name), mode)
        imgs += [img]
    return imgs

def correct_vals(images, keep_val, keep=1, toss=0):
    for i in range(len(images)):
        images[i][images[i] != keep_val] = toss
        images[i][images[i] == keep_val] = keep
    return images

def make_segmentation_labels(images, classes=[0,1]):
    num_images, rows, cols = images.shape
    labels = []

    for c_index in classes:
        label_ch = np.zeros((num_images, rows, cols))
        label_ch[images == c_index] = 1
        labels += [np.expand_dims(label_ch, axis=-1)]

    return np.concatenate(labels, axis=-1)

def correct_shapes(images, std_shape=(240, 320)):
    for i in range(len(images)):
        if images[i].shape[:2] != std_shape:
            images[i] = cv2.resize(images[i], std_shape[::-1])
    return images

def normalize_image(image, rows=240, cols=320):
    image = correct_shapes([image], (rows, cols))
    return image[0].astype("float32") / 255.0

def normalize_groundtruth(image, rows=240, cols=320):
    image = correct_vals([image], 255)
    image = correct_shapes(image)
    return image[0].astype("int32")

def load_normalized_input(folder, subfolder="/input"):
    images = load_folder("./" + folder + subfolder, 1)
    images = correct_shapes(images)
    images = np.array(images).astype("float32") / 255.0
    return images

def load_normalized_roi(folder, subfolder="/groundtruth"):
    gt = load_folder("./" + folder + subfolder, 0)
    gt = correct_vals(gt, 85)
    gt = correct_shapes(gt)
    gt = np.array(gt).astype("int32")
    return gt

def load_normalized_groundtruth(folder, subfolder="/groundtruth"):
    gt = load_folder("./" + folder + subfolder, 0)
    gt = correct_vals(gt, 255)
    gt = correct_shapes(gt)
    gt = np.array(gt).astype("int32")
    return gt

def load_normalized(folder):
    images = load_normalized_input(folder)
    gt = load_normalized_groundtruth(folder)

    return images, gt

if __name__ == "__main__":
    folder = "collective" if len(sys.argv) < 2 else sys.argv[1]
    images, gt = load_normalized(folder)

    np.save("./" + folder + "/input.npy", images)
    np.save("./" + folder + "/groundtruth.npy", gt)
