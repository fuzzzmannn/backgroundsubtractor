import os, os.path
import numpy as np
import cv2

def load_folder(path, limit=-1):
    imgs = []

    for i, name in enumerate(os.listdir(path)):
        if limit >= 0 and i >= limit:
            break
        imgs += [cv2.imread(os.path.join(path, name), 1)]
    return imgs

def show_imgs(batch):
    for i in range(batch.shape[0]):
        img = cv2.applyColorMap(batch[i].astype('uint8'), cv2.COLORMAP_JET)
        cv2.imshow("fart", img)
        cv2.waitKey(1)

def med_stack(batch):
    channel_medians = []

    for i in range(batch.shape[-1]):
        med_img = np.median(batch[..., i], axis=0).astype('uint8')
        channel_medians += [np.expand_dims(med_img, axis=-1)]

    return np.concatenate(channel_medians, axis=-1)


if __name__ == '__main__':
    imgs = np.array(load_folder("./highway/groundtruth"))#, 300))
    show_imgs(imgs)
    # med_img = med_stack(imgs)
    #
    # cv2.imshow("fart", med_img)
    # cv2.waitKey(0)
