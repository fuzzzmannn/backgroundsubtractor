import os
import sys
import cv2
import skimage

import numpy as np
import FileManager as fm

from BackgroundModel import BackgroundModel
from BackgroundModel import do_update_step

# copied from https://gist.github.com/hasnainv/49dc4a85933de6b979f8
def extract_patches(image, window_shape=(13,13, 3), stride=3):
    nr, nc, ncolor = image.shape
    patches = skimage.util.view_as_windows(image, window_shape, stride)
    nR, nC, t, H, W, C = patches.shape
    nWindow = nR * nC
    patches =  np.reshape(patches, (nWindow, H, W, C))
    patches = patches.astype(np.float32, copy=False)
    return patches

# assuming a image shape of [240,320]
def make_patch_tuple(image, background, groundtruth):
    i_padded = np.pad(image, ((16,16),(16,16),(0,0)), 'constant', constant_values=0)
    i_patches = extract_patches(i_padded, (32,32,3), 8)

    b_padded = np.pad(background, ((16,16),(16,16),(0,0)), 'constant', constant_values=0)
    b_patches = extract_patches(b_padded, (32,32,3), 8)

    g_padded = np.pad(groundtruth, ((16,16),(16,16),(0,0)), 'constant', constant_values=0)
    g_patches = extract_patches(g_padded, (32,32,1), 8)

    return i_patches, b_patches, g_patches

def get_roi(folder_path, roi_filename):
    filename = os.path.join(folder_path, roi_filename)
    start, end = open(filename, 'r').read().split()
    return int(start), int(end)

def get_random_indexes(roi_length, roi_start, num_frames):
    indexes = np.arange(roi_length) + roi_start
    np.random.shuffle(indexes)
    return np.sort(indexes[:num_frames])

if __name__ == "__main__":
    folder = "highway" if len(sys.argv) < 2 else sys.argv[1]
    start, end = get_roi("./" + folder, "temporalROI.txt")

    length = end-start
    num_frames = int(length * .05)
    rand_indexes = get_random_indexes(length, start, num_frames)

    groundtruth = np.load("./" + folder + "/groundtruth.npy")
    groundtruth = np.expand_dims(groundtruth, axis=-1)

    images = np.load("./" + folder + "/input.npy")
    batch_size, rows, cols, channels = images.shape
    median = fm.med_stack(images[:start])

    model_depth = 40
    model = BackgroundModel(rows, cols, channels, model_depth)
    model.update_library(median, np.zeros((rows, cols)))


    for i in range(batch_size):
        background = do_update_step(images[i], median, model)

        cv2.imshow("frame", np.hstack([images[i], background]))
        cv2.waitKey(1)

        if i == rand_indexes[0]:
            rand_indexes = rand_indexes[1:]

            patch_tuple = make_patch_tuple(images[i], background, groundtruth[i])
            patch_dict = {"images":patch_tuple[0],"backgrounds":patch_tuple[1],"masks":patch_tuple[2]}
            np.save("./" + folder + "/patches" + str(i) + ".npy", patch_dict)
            print("saved patches for frame", i)
            patch_dict = None
            patch_tuple = None

        if rand_indexes.shape[0] == 0:
            break
