import image_utils as util
from random import shuffle
import numpy as np
import time as t
import cv2

# Memory-efficient batch manager class that loads data only as batches are
# requested. I haven't quite worked out how to whiten the data in this yet,
# because you still need to load all of the data into memory at some point to
# do that.
class BatchManager:
    """
    Memory-efficient batch manager that loads data only as batches are
    requested. Does not whiten data yet.
    """
    def __init__(self, inputs, labels, batch_size, scale=2, hcrop=0, vcrop=0):
        """
        Initializes a MatlabBatchManager given a set of input and label
        filenames.

        Parameters
        ----------
        inputs : [string]
            List of paths to the input data files.
        labels : [string]
            List of paths to the label data files.
        batch_size : int
            Desired size of each batch. The manager will attempt to break the
            data evenly into batches of the given size. Behavior is undefined
            if the size of the dataset is not evenly divisible by batch_size.
        scale : int
            Scales the data and labels down by a power of 2. A scale of 2
            downsamples the images and labels to 1/4 of the original scale.
            Defaults to 2.
        """
        self.num_instances = inputs.shape[0]
        self.num_batches = int(inputs.shape[0] / batch_size)
        self.inputs = np.split(inputs, self.num_batches)
        self.labels = np.split(labels, self.num_batches)
        self.batch_counter = 0
        self.scale = scale
        self.hcrop = hcrop
        self.vcrop = vcrop

        scale_factor = int(2**self.scale)
        example_input = self.load_data([self.inputs[0][0]], 1)[0]
        self.input_shape = np.array(example_input.shape)

        example_label = np.expand_dims(self.load_data([self.labels[0][0]], 0)[0], axis=-1)
        self.label_shape = np.array(example_label.shape)

    # automatically scales down and center crops image unless indicated otherwise
    def load_data(self, filenames, color_mode=1, do_scale=True, center_crop=True):
        images = []
        for f in filenames:
            # check if input is a numpy file, otherwise loads as image
            if '.npy' in f:
                images.append(np.load(f))
            else:
                images.append(cv2.imread(f, color_mode))

            if center_crop == True and self.hcrop * self.vcrop > 0:
                images[-1] = util.center_crop(images[-1], self.vcrop, self.hcrop)
            if do_scale == True:
                images[-1] = util.scale_down(images[-1], self.scale)
        return images

    def save(self, out_path):
        import json
        with open(out_path, 'w') as f:
            dataset_dict = {"inputs": np.concatenate(self.inputs).tolist(),
                            "labels": np.concatenate(self.labels).tolist()}
            json.dump(dataset_dict, f)

    def reset(self):
        """Reshuffle the data into new batches"""
        inputs = np.concatenate(self.inputs)
        labels = np.concatenate(self.labels)
        shuffle(inputs, labels, int(t.time()))
        self.inputs = np.split(inputs, self.num_batches)
        self.labels = np.split(labels, self.num_batches)

        self.batch_counter = 0

    def batches_remaining(self):
        return self._num_batches - self.batch_counter

    def get_entire_set(self):
        inputs = np.concatenate(self.inputs)
        labels = np.concatenate(self.labels)
        return rescale_data(np.array(self.load_data(inputs, 1))), rescale_data(np.array(self.load_data(labels, 0)))

    def augment_image(self, image, depth):
        if np.random.randint(0,2) == 0:
            gamma = np.random.uniform(.8, 1.2)
            image = util.adjust_gamma(image, gamma)
            brightness = np.random.uniform(.5, 2.0)
            image = util.adjust_brightness(image, brightness)
            color = np.random.uniform(.8, 1.2, 3)
            image = util.adjust_color(image, color[0], color[1], color[2])
        if np.random.randint(0,2) == 0:
            image = np.fliplr(image)
            depth = np.fliplr(depth)
        return image, depth

    def next_batch(self, augment=True):
        """Returns the next consecutive unused batch from the dataset"""
        """Randomly flips images and depths horizontally to agument data"""
        if self.batch_counter >= self.num_batches: self.reset()

        input_imgs = self.load_data(self.inputs[self.batch_counter], 1, not augment, not augment)
        label_imgs = self.load_data(self.labels[self.batch_counter], 0, not augment, not augment)

        if augment == True:
            for i in range(len(input_imgs)):
                seed = int(t.time())
                image, label = self.augment_image(input_imgs[i], label_imgs[i])
                image = util.random_crop(image, self.vcrop, self.hcrop, seed)
                image = util.scale_down(image, self.scale)
                label = util.random_crop(label, self.vcrop, self.hcrop, seed)
                label = util.scale_down(label, self.scale)

                input_imgs[i] = image
                label_imgs[i] = label

        self.batch_counter += 1
        return np.array(input_imgs), np.array(label_imgs)

def rescale_data(images):
    return images.astype('float32')/255.0#/128.0 - 1

def load_dataset(json_path, batch_size, scale=2):
    import json

    with open(json_path, 'r') as f:
        dataset = json.load(f)
        return BatchManager(dataset['inputs'], dataset['labels'], batch_size, scale)

# divides the dataset into a test/training split, with a batch manager for each
def divide_dataset(inputs_path, labels_path, batch_size, scale=2, split=.7,
                    hcrop=0, vcrop=0, split_seed=None, test_size=None):
    """
    Divides the dataset by a test/training split and returns a BatchManager for
    each.

    Inputs
    ----------
    inputs_path : str
        Relative path to the directory containing input images.
    labels_path : str
        Relative path to the directory containing the depth labels.
    batch_size : int
        The desired size of each batch. Should evenly divide the dataset.
    scale : int
        Scales the data and labels down by a power of 2. Defaults to 2.
    split : float
        Defines the test/training split. Defaults to .7 (for .7 training, .3
        test)

    Returns
    -------
    training_manager : BatchManager
        BatchManager for the training data set.
    test_manager : BatchManager
        BatchManager for the test data set.
    """
    from os import listdir
    from os.path import isfile, join

    inputs = np.array([join(inputs_path, f) for f in listdir(inputs_path) if isfile(join(inputs_path, f))])
    inputs.sort()
    labels = np.array([join(labels_path, f) for f in listdir(labels_path) if isfile(join(labels_path, f))])
    labels.sort()

    split_seed = int(t.time()) if split_seed is None else split_seed
    shuffle(inputs, labels, split_seed)
    split_index = int(inputs.shape[0] * split)
    if test_size is not None:
        split_index = inputs.shape[0] - test_size
    print "split_index", split_index

    training_manager = BatchManager(inputs[0:split_index], labels[0:split_index], batch_size, scale, hcrop, vcrop)
    test_manager = BatchManager(inputs[split_index:], labels[split_index:], batch_size, scale, hcrop, vcrop)

    return training_manager, test_manager

def load_no_labels(path, batch_size, scale=2, split=.7, hcrop=0, vcrop=0):
    from os import listdir
    from os.path import isfile, join

    inputs = np.array([join(inputs_path, f) for f in listdir(inputs_path) if isfile(join(inputs_path, f))])

    return BatchManager(inputs, labels, batch_size, scale, hcrop, vcrop)

def shuffle(inputs, labels, seed):
    """helper function shuffles the input and labels arrays in place"""
    np.random.seed(seed)
    np.random.shuffle(inputs)
    np.random.seed(seed)
    np.random.shuffle(labels)
