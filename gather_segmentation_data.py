import os
import sys
from shutil import copyfile

def get_roi(folder_path, roi_filename):
    filename = os.path.join(folder_path, roi_filename)
    start, end = open(filename, 'r').read().split()
    return int(start), int(end)

def get_copy_name(filename, base_name, index):
    ext = filename.split(".")[1]
    return str(base_name + "%06d." + ext) % index

def copy_roi_data(folder_path, dest_path, start_id,
    input_name="input", gt_name="groundtruth", roi_filename="temporalROI.txt"):
    input_folder = os.path.join(folder_path, input_name)
    gt_folder = os.path.join(folder_path, gt_name)

    input_dest_folder = os.path.join(dest_path, input_name)
    gt_dest_folder = os.path.join(dest_path, gt_name)

    start, end = get_roi(folder_path, roi_filename)
    input_files = next(os.walk(input_folder))[2][start-1:end+1]
    gt_files = next(os.walk(gt_folder))[2][start-1:end+1]
    print(input_files[0], input_files[-1])
    print(gt_files[0], gt_files[-1])

    for i in range(len(input_files)):
        input_path = os.path.join(input_folder, input_files[i])
        gt_path = os.path.join(gt_folder, gt_files[i])

        input_copy_name = get_copy_name(input_files[i], "input", start_id + i)
        gt_copy_name = get_copy_name(gt_files[i], "gt", start_id + i)

        copyfile(input_path, os.path.join(input_dest_folder, input_copy_name))
        copyfile(gt_path, os.path.join(gt_dest_folder, gt_copy_name))

    # return the next id in sequence
    return start_id + len(input_files) + 1


if __name__ == "__main__":
    dest_folder = "collective" if len(sys.argv) < 2 else sys.argv[1]
    search_folder = None if len(sys.argv) < 3 else sys.argv[2]
    next_id = 0

    search_folders = [search_folder] if search_folder is not None else next(os.walk("./"))[1]
    for folder in search_folders:
        if folder == dest_folder or folder == "_pycache_": continue
        print("in", folder)

        next_id = copy_roi_data("./" + folder, "./" + dest_folder, next_id)

        print("")
