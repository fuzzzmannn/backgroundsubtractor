import tensorflow as tf
import numpy as np
import cv2

import os
import sys

from BackgroundSubtractor import BackgroundSubtractor
from BackgroundModel import close

import data_loader as data

def shuffle_batch(batch, seed=-1):
    if seed >= 0: np.random.seed(seed)
    np.random.shuffle(batch)

if __name__ == "__main__":
    os.environ['CUDA_VISIBLE_DEVICES'] = '0' if len(sys.argv) < 2 else sys.argv[1]
    # network_filename = './_models/bg_subtractor_with_depth.ckpt' if "resume" in sys.argv else None
    network_filename = './_models/bg_subtractor.ckpt' if "resume" in sys.argv else None
    test_only = "test" in sys.argv
    output_folder = "./_models"
    batch_size = 124

    rows = 240 if test_only else 32
    cols = 320 if test_only else 32
    network = BackgroundSubtractor(rows=rows, cols=cols, in_channels=6, network_filename=network_filename)

    if test_only == False:
        # load training and test data
        train_set = data.open_dataset("./dataset.json")
        test_set = data.open_dataset("./testset.json")
        train_length = data.get_max_length(train_set)
        test_length = data.get_max_length(test_set)
        data.shuffle_dataset(train_set)

        # training loop
        least_validation_loss = float("inf")

        print("training")
        for epoch in range(1000):
            # training epoch
            losses = []
            for i in range(train_length):
                i_patches, b_patches, m_patches, d_patches, g_patches = data.get_patch_tuple(train_set, i, True)
                # d_patches = np.expand_dims(d_patches, axis=-1)

                # groups = []
                # for i in range(100, 120):
                #     g_stack = np.dstack([g_patches[i], g_patches[i], g_patches[i]])
                #     groups += [np.hstack([i_patches[i], b_patches[i], g_stack])]
                # tiles = np.vstack(groups)
                # cv2.imwrite("tiles.png", tiles)
                # cv2.imshow("fart", tiles)
                # cv2.waitKey(0)
                # quit()

                # i_patches = np.concatenate([i_patches, b_patches, d_patches], axis=-1)
                i_patches = np.concatenate([i_patches, b_patches], axis=-1)
                b_patches = None
                m_patches = None
                d_patches = None

                divisions = int(i_patches.shape[0]/batch_size)
                for j in range(divisions):
                    images = i_patches[j * batch_size : j * batch_size + batch_size]
                    labels = g_patches[j * batch_size : j * batch_size + batch_size]
                    fdict = {network.input_data:images, network.input_labels:labels}
                    network.train_segmentation.run(feed_dict=fdict)
                    losses += [network.segmentation_loss.eval(feed_dict=fdict)]

            print("training epoch", epoch, np.mean(losses))

            # test epoch
            losses = []
            output_images = []
            for i in range(test_length):
                i_patches, b_patches, m_patches, d_patches, g_patches = data.get_patch_tuple(test_set, i, True)
                # d_patches = np.expand_dims(d_patches, axis=-1)
                i_patches = np.concatenate([i_patches, b_patches], axis=-1)
                # i_patches = np.concatenate([i_patches, b_patches, d_patches], axis=-1)
                b_patches = None
                m_patches = None
                d_patches = None

                divisions = int(i_patches.shape[0]/batch_size)
                for j in range(divisions):
                    images = i_patches[j * batch_size : j * batch_size + batch_size]
                    labels = g_patches[j * batch_size : j * batch_size + batch_size]
                    fdict = {network.input_data:images, network.input_labels:labels}
                    output, loss = network.sess.run([network.prediction, network.segmentation_loss], feed_dict=fdict)
                    losses += [network.segmentation_loss.eval(feed_dict=fdict)]

                    if len(output_images) < 10:
                        index = np.random.randint(0, output.shape[0])
                        guess_gt_pair = np.hstack([np.squeeze(output[index]), np.squeeze(labels[index])])
                        output_images += [guess_gt_pair]

            validation_loss = np.mean(losses)
            print("test epoch", epoch, validation_loss)

            output_images = np.vstack(output_images).astype('uint8') * 255
            cv2.imshow("segmentation", output_images)
            cv2.waitKey(1)

            if validation_loss < least_validation_loss:
                least_validation_loss = validation_loss
                print("saving best...")
                network.saver.save(network.sess, "./_models/bg_subtractor_with_depth.ckpt")

            print("")

            data.shuffle_dataset(train_set)
            data.shuffle_dataset(test_set)
    else:
        print("test")
        from scipy.signal import medfilt

        validation = data.open_dataset("./validation.json")

        for folder in validation:
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            out = cv2.VideoWriter(folder + '_segmentation.avi', fourcc, 30.0, (320*2,240*2))

            if not os.path.exists("./" + folder + "/segment"):
                os.makedirs("./" + folder + "/segment")

            for i in range(len(validation[folder])):
                image, background, mask, depth, gt = data.load(validation, folder, i)
                # mask = np.expand_dims(mask, axis=-1)
                # depth = np.expand_dims(depth, axis=-1)
                # image = np.expand_dims(np.dstack([image, background, depth]), axis=0)
                # image = np.expand_dims(np.dstack([image, background, mask]), axis=0)
                image = np.expand_dims(np.dstack([image, background]), axis=0)
                background = None
                mask = None
                depth = None
                output = network.sess.run(network.prediction, feed_dict={network.input_data:image})[0]
                output = np.pad(np.squeeze(output), ((4,4),(4,4)), 'edge').astype(np.uint8)
                output = np.expand_dims(close(medfilt(output, 7))[4:-4, 4:-4], axis=-1)
                # output = np.dstack([output, output, output])

                gt = np.zeros_like(output) if gt is None else np.dstack([gt, gt, gt])

                upper = np.hstack([image[0][...,:3], image[0][...,3:]])#-1]])
                lower = np.hstack([output, gt])
                frame = (np.vstack([upper, lower]) * 255.0).astype(np.uint8)

                frame_fname = str("./" + folder + "/segment/sg%06d.png") % (i+1)
                cv2.imwrite(frame_fname, np.clip(output * 255.0, 0.0, 255.0).astype(np.uint8))
                out.write(frame)
                cv2.imshow("frame", frame)
                cv2.waitKey(1)

            out.release()
