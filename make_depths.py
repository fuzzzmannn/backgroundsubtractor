import numpy as np
import cv2
import sys
import os

from normalize_data import load_normalized_input
from PytorchDepthNetwork import DepthNetwork

def predict_depth(network, image):
    image = np.expand_dims(image, axis=0)
    output = network.forward(image)[0]# network.evaluate_rgb(image)[0][0]
    return np.expand_dims(np.squeeze(output.data[0].cpu().numpy()), axis=-1)

def write_depth_maps(folder, network):
    # normalize images
    batch_images = load_normalized_input(folder, "/input")
    batch_images = np.expand_dims(batch_images, axis=1)
    batch_images = np.squeeze(np.transpose(batch_images, (0,4,2,3,1)), axis=-1)

    # create the output directory
    if not os.path.exists("./" + folder + "/depths"):
        os.makedirs("./" + folder + "/depths")

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(folder + '_depth.avi', fourcc, 30.0, (320,240))

    for i in range(batch_images.shape[0]):
        depth = predict_depth(network, batch_images[i])
        depth = np.clip(depth, 0.0, 255.0).astype(np.uint8)
        depth = np.dstack([depth, depth, depth])
        display = cv2.applyColorMap(depth[..., 0], colormap=cv2.COLORMAP_HOT)

        depth_fname = str("./" + folder + "/depths/mk%06d.png") % (i+1)
        cv2.imshow("stuff", display)
        out.write(display)
        cv2.imwrite(depth_fname, depth)
        cv2.waitKey(1)
    out.release()

def load_checkpoint(filename):
    import torch

    state = torch.load(filename)
    return state['network_state']

if __name__ == "__main__":
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    folders = sys.argv[1:]

    rows, cols, rgb_channels, depth_channels = (240, 320, 3, 2)
    network_state = load_checkpoint("./ModelBackup/checkpoint")
    network = DepthNetwork(rgb_channels, depth_channels * 2 + 1, 1, network_state)
    # network = DepthNetwork(rows, cols, rgb_channels, depth_channels,
    #                         model_filename="./ModelBackup/depthlearner.ckpt")

    for folder in folders:
        print("in", folder)
        write_depth_maps(folder, network)
