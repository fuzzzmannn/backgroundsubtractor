import numpy as np
import cv2
import sys
import os

from BackgroundModel import get_diff_mask
from normalize_data import load_normalized_input

def write_segmentation_masks(folder):
    foregrounds = load_normalized_input(folder, "/input")
    backgrounds = load_normalized_input(folder, "/background")

    # create the output directory
    if not os.path.exists("./" + folder + "/masks"):
        os.makedirs("./" + folder + "/masks")

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(folder + '_mask.avi', fourcc, 30.0, (320,240))

    for i in range(foregrounds.shape[0]):
        mask = get_diff_mask(foregrounds[i], backgrounds[i])
        mask = (np.dstack([mask, mask, mask]) * 255).astype(np.uint8)

        mask_fname = str("./" + folder + "/masks/mk%06d.png") % (i+1)
        cv2.imwrite(mask_fname, mask)
        out.write(mask)
        cv2.imshow("stuff", mask)
        cv2.waitKey(1)
    out.release()

if __name__ == "__main__":
    folders = sys.argv[1:]

    for folder in folders:
        print("in", folder)
        write_segmentation_masks(folder)
