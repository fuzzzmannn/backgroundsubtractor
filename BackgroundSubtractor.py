import tensorflow as tf
from unet3_model import unet_small

class BackgroundSubtractor:
    def __init__(self, rows=32, cols=32, in_channels=7, out_classes=2,
                learning_rate=.001, network_filename=None):
        tf.reset_default_graph()
        self.sess = tf.InteractiveSession()

        # assemble placeholders
        input_shape = [None, rows, cols, in_channels]
        label_shape = [None, rows, cols]

        self.input_data = tf.placeholder(tf.float32, input_shape)
        self.input_labels = tf.placeholder(tf.int32, label_shape)

        # assemble network and loss function
        with tf.name_scope("model"):
            # predicts foreground and background
            self.logits = unet_small(self.input_data, "segmentation", out_classes)
            prediction_shape = tf.shape(self.logits)

        with tf.name_scope("cost_function"):
            self.flat_logits = tf.reshape(self.logits, (-1, out_classes))
            self.train_labels = tf.reshape(self.input_labels, [-1])
            self.cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.flat_logits, labels=self.train_labels, name="x_ent")
            self.segmentation_loss = tf.reduce_mean(self.cross_entropy, name="x_ent_mean")

            self.prediction = tf.argmax(tf.reshape(tf.nn.softmax(self.logits), prediction_shape), dimension=3)

        self.optimizer = tf.train.AdamOptimizer(learning_rate)
        self.train_segmentation = self.optimizer.minimize(self.segmentation_loss)

        # load old weights
        self.saver = tf.train.Saver()
        if network_filename is not None:
            self.saver.restore(self.sess, network_filename)
            print("loaded network weights")
        else:
            self.sess.run(tf.global_variables_initializer())
