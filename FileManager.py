import os, os.path
import numpy as np
import cv2

def load_folder(path, limit=-1):
    imgs = []

    for i, name in enumerate(os.listdir(path)):
        if limit >= 0 and i >= limit:
            break
        imgs += [cv2.imread(os.path.join(path, name), 1)]
    return np.array(imgs)

def med_stack(batch):
    channel_medians = []

    for i in range(batch.shape[-1]):
        med_img = np.median(batch[..., i], axis=0)
        channel_medians += [np.expand_dims(med_img, axis=-1)]

    return np.concatenate(channel_medians, axis=-1)
