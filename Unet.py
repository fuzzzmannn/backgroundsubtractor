import torch
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

class MaxPool2d(nn.Module):
    def __init__(self, kernel_size=2, stride=2, same_padding=True):
        super(MaxPool2d, self).__init__()
        self.same_padding = same_padding
        self.stride = stride
        self.pool = nn.MaxPool2d(kernel_size=kernel_size, stride=stride)

    def forward(self, x):
        pad_dim2 = x.size(2) % self.stride > 0
        pad_dim3 = x.size(3) % self.stride > 0
        pad1 = [0,0] if pad_dim3 == False else [0,1]
        pad2 = [0,0] if pad_dim2 == False else [0,1]

        x = self.pool(x)
        x = F.pad(x, pad1 + pad2)
        return x

class Conv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=3, padding=True, batch_norm=True):
        super(Conv2d, self).__init__()
        pad_size = int(kernel_size/2.0)

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size, padding=pad_size)
        self.norm = None if batch_norm == False else nn.BatchNorm2d(out_channels)

    def forward(self, x):
        x = F.relu(self.conv(x))
        return x if self.norm is None else self.norm(x)

class Upsample(nn.Module):
    def __init__(self, in_channels, out_channels, scale_factor=2):
        super(Upsample, self).__init__()

        self.upsample = nn.Upsample(scale_factor=scale_factor, mode='bilinear')
        self.conv = Conv2d(in_channels, out_channels)

    def forward(self, x):
        x = self.upsample(x)
        return self.conv(x)

class Unet(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Unet, self).__init__()

        self.conv1 = Conv2d(in_channels, 64)
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = Conv2d(64, 128)
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = Conv2d(128, 256)
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv4 = Conv2d(256, 512)
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv5 = Conv2d(512, 1024)
        self.conv6 = Conv2d(1024, 1024)

        self.upsample1 = Upsample(1024, 512)
        self.skipconv1 = Conv2d(1024, 512)

        self.upsample2 = Upsample(512, 256)
        self.skipconv2 = Conv2d(512, 256)

        self.upsample3 = Upsample(256, 128)
        self.skipconv3 = Conv2d(256, 128)

        self.upsample4 = Upsample(128, 64)
        self.skipconv4 = Conv2d(128, 64)

        self.conv7 = Conv2d(64, out_channels)

    def pad_to_match(self, x, y):
        row_diff = y.size(2) - x.size(2)
        col_diff = y.size(3) - x.size(3)

        pad_dim2 = int(row_diff if row_diff % 2 != 0 else row_diff / 2)
        pad_dim3 = int(col_diff if col_diff % 2 != 0 else col_diff / 2)

        pad1 = [pad_dim3,pad_dim3] if pad_dim3 % 2 == 0 else [0,pad_dim3]
        pad2 = [pad_dim2,pad_dim2] if pad_dim2 % 2 == 0 else [0,pad_dim2]
        x = F.pad(x, pad1 + pad2)
        return x

    def forward(self, x):
        # print(x.size())
        connection1 = F.relu(self.conv1(x))
        x = self.pool1(connection1)
        # print(x.size())

        connection2 = F.relu(self.conv2(x))
        x = self.pool2(connection2)
        # print(x.size())

        connection3 = F.relu(self.conv3(x))
        x = self.pool3(connection3)
        # print(x.size())

        connection4 = F.relu(self.conv4(x))
        x = self.pool4(connection4)
        # print(x.size())

        x = F.relu(self.conv5(x))
        x = F.relu(self.conv6(x))
        # print(x.size())

        x = self.upsample1(x)
        # print(x.size())

        x = self.pad_to_match(x, connection4)
        # print(x.size())

        x = torch.cat((x, connection4), dim=1)
        x = self.skipconv1(x)
        # print(x.size())

        x = self.upsample2(x)
        x = torch.cat((x, connection3), dim=1)
        x = self.skipconv2(x)
        # print(x.size())

        x = self.upsample3(x)
        x = torch.cat((x, connection2), dim=1)
        x = self.skipconv3(x)
        # print(x.size())

        x = self.upsample4(x)
        x = torch.cat((x, connection1), dim=1)
        x = self.skipconv4(x)
        # print(x.size())

        x = self.conv7(x)
        return x
