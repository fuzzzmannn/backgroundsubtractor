import BatchManager as bm
import numpy as np
import cv2
import sys
import os
import time

from PytorchDepthNetwork import DepthNetwork

def make_uint8_img(img):
    return np.clip(img * 255.0, 0.0, 255.0).astype('uint8')

# TBD: add functionality to save last best error rate
def save_checkpoint(epoch, train_combined, split_seed, test_size, best_error, network_state, filename):
    import torch
    state = {'epoch': epoch,
             'train_combined': train_combined,
             'split_seed': split_seed,
             'test_size': test_size,
             'network_state': network_state,
             'best_error': best_error}
    torch.save(state, filename)

def load_checkpoint(filename):
    import torch

    state = torch.load(filename)
    return state['epoch'], state['train_combined'], state['split_seed'], state['test_size'], state['best_error'], state['network_state']

if __name__ == "__main__":
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'

    test_only = "test" in sys.argv
    load_model = "resume" in sys.argv
    network_state = None
    model_filename = "./ModelBackup/checkpoint.pth.tar"

    prev_best = float("inf")
    start_epoch = 0
    train_combined = False
    split_seed = int(time.time())
    test_size = 750

    # load training and test data
    batch_size = 10
    crop_size = 32

    if load_model == True:
        # network_state = load_checkpoint(model_filename)
        start_epoch, train_combined, split_seed, test_size, prev_best, network_state = load_checkpoint(model_filename)
        start_epoch += 1
        print("loaded!")

    train, test = bm.divide_dataset("FromCamera3/images/", "FromCamera3/depths/",
            batch_size=batch_size, hcrop=crop_size, vcrop=crop_size,
            split_seed=split_seed, test_size=test_size, scale=1)

    rows, cols, rgb_channels = train.input_shape
    depth_channels = 1
    learning_rate = .001
    network = DepthNetwork(rgb_channels, depth_channels * 2 + 1, depth_channels, network_state)

    if test_only == False:
        print("training")
        for epoch in range(start_epoch, 800):
            losses = []

            for i in range(train.num_batches):
                batch_images, batch_labels = train.next_batch()

                # normalize images
                batch_images = batch_images.astype('float32') / 255.0
                batch_images = np.expand_dims(batch_images, axis=1)
                batch_images = np.squeeze(np.transpose(batch_images, (0,4,2,3,1)), axis=-1)

                batch_labels = np.expand_dims(batch_labels, axis=1)
                batch_depths = None if train_combined == False else batch_labels

                depth_mask = np.zeros_like(batch_labels)
                depth_mask[batch_labels > 0.0] = 1.0

                out1, out2, loss = network.train(batch_images, batch_depths, batch_labels, depth_mask)
                losses += [loss.data.cpu().numpy()[0]]

            print "training epoch", epoch, np.mean(losses)
            predictions = []
            losses = []

            for i in range(test.num_batches):
                batch_images, batch_labels = test.next_batch(augment=False)

                # normalize images
                batch_images = batch_images.astype('float32') / 255.0
                batch_images = np.expand_dims(batch_images, axis=1)
                batch_images = np.squeeze(np.transpose(batch_images, (0,4,2,3,1)), axis=-1)

                batch_labels = np.expand_dims(batch_labels, axis=1)
                batch_depths = None if train_combined == False else batch_labels

                depth_mask = np.zeros_like(batch_labels)
                depth_mask[batch_labels > 0.0] = 1.0

                out1, out2, loss = network.forward(batch_images, batch_depths, batch_labels, depth_mask)
                losses += [loss.data.cpu().numpy()[0]]

                if i % int(test.num_batches / 4) == 0:
                    out1 = np.expand_dims(np.squeeze(out1.data[0].cpu().numpy()), axis=-1)
                    if out2 is not None:
                        out2 = np.expand_dims(np.squeeze(out2.data[0].cpu().numpy()), axis=-1)

                    # flip back channel order
                    prediction = out1 if out2 is None else np.hstack([out1, out2])
                    prediction = cv2.applyColorMap(make_uint8_img(prediction), colormap=cv2.COLORMAP_HOT)

                    label = np.expand_dims(np.squeeze(batch_labels[0]), axis=-1)
                    label = cv2.applyColorMap(make_uint8_img(label), colormap=cv2.COLORMAP_HOT)

                    image = np.transpose(np.expand_dims(batch_images[0], axis=-1), (3,1,2,0))
                    image = make_uint8_img(np.squeeze(image))

                    out = np.hstack([image, prediction, label])
                    predictions += [out]

            test_loss = np.mean(losses)
            out_image = np.vstack(predictions)

            print "test epoch", test_loss
            cv2.imshow("prediction", out_image)
            cv2.waitKey(1)

            if test_loss < prev_best:
                prev_best = test_loss
                network_state = network.get_state()
                save_checkpoint(epoch, train_combined, split_seed, test_size, prev_best, network_state, model_filename)
                print "saved best"
            print ""
    else:
        print("testing")
        num_out = 5
        predictions = []
        losses = []

        for i in range(test.num_batches):
            batch_images, batch_labels = test.next_batch(augment=False)

            # normalize images
            batch_images = batch_images.astype('float32') / 255.0
            batch_images = np.expand_dims(batch_images, axis=1)
            batch_images = np.squeeze(np.transpose(batch_images, (0,4,2,3,1)), axis=-1)

            batch_labels = np.expand_dims(batch_labels, axis=1)
            batch_depths = None if train_combined == False else batch_labels

            depth_mask = np.zeros_like(batch_labels)
            depth_mask[batch_labels > 0.0] = 1.0

            out1, out2, loss = network.forward(batch_images, batch_depths, batch_labels, depth_mask)
            losses += [loss.data.cpu().numpy()[0]]

            if i % int(test.num_batches / num_out) == 0:
                out1 = np.expand_dims(np.squeeze(out1.data[0].cpu().numpy()), axis=-1)
                if out2 is not None:
                    out2 = np.expand_dims(np.squeeze(out2.data[0].cpu().numpy()), axis=-1)

                # flip back channel order
                prediction = out1 if out2 is None else np.hstack([out1, out2])
                prediction = cv2.applyColorMap(make_uint8_img(prediction), colormap=cv2.COLORMAP_HOT)

                label = np.expand_dims(np.squeeze(batch_labels[0]), axis=-1)
                label = cv2.applyColorMap(make_uint8_img(label), colormap=cv2.COLORMAP_HOT)

                image = np.transpose(np.expand_dims(batch_images[0], axis=-1), (3,1,2,0))
                image = make_uint8_img(np.squeeze(image))

                out = np.hstack([image, prediction, label])
                predictions += [out]

        test_loss = np.mean(losses)
        out_image = np.vstack(predictions)

        print "test epoch", test_loss
        cv2.imshow("prediction", out_image)
        cv2.waitKey(0)
