import os
from os.path import isfile
import numpy as np
import cv2

path = "./NYU_Depth/images/"

def make_mask(image, val=0):
    return image == val

def random_crop(image, rows=32, cols=32, seed=None):
    np.random.seed(seed)
    top = 0 if rows == 0 else np.random.randint(0, rows)
    bottom = -1 if rows == 0 else -(rows-top)

    left = 0 if cols == 0 else np.random.randint(0, cols)
    right = -1 if cols == 0 else -(cols-left)

    image = image[top:bottom, left:right]
    return image

def center_crop(image, rows, cols):
    hrows = int(rows/2)
    hcols = int(cols/2)
    return image[hrows:-hrows if hrows > 0 else -1, hcols:-hcols if hcols > 0 else -1]

def scale_down(image, scale=0):
    if scale == 0: return image
    for i in range(scale):
        image = cv2.pyrDown(image)
    return image

def adjust_color(image, rshift=1.0, gshift=1.0, bshift=1.0):
    image = image.astype("float32")
    image[:,:,0] *= bshift
    image[:,:,1] *= gshift
    image[:,:,2] *= rshift
    image = np.clip(image, 0.0, 255.0)
    return image.astype("uint8")

def adjust_brightness(image, brightness=1.0):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV).astype("float32")
    image[:,:,-1] *= brightness
    image = np.clip(image, 0.0, 255.0)
    return cv2.cvtColor(image.astype("uint8"), cv2.COLOR_HSV2BGR)

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")

	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)


if __name__ == "__main__":
    for name in os.listdir(path):
        if ".png" not in name.lower(): continue

        img = cv2.imread(path + name, 1)
        # img = random_crop(img)
        # print(img.shape)
        # img = cv2.resize(img, (240, 160), interpolation = cv2.INTER_CUBIC)

        gamma = np.random.uniform(.8, 1.2)
        brightness = np.random.uniform(.5, 2.0)
        color = np.random.uniform(.8, 1.2, 3)
        img = adjust_gamma(img, gamma)
        img = adjust_brightness(img, brightness)
        img = adjust_color(img, color[0], color[1], color[2])

        cv2.imshow("gamma shifted", img)
        cv2.waitKey(0)
