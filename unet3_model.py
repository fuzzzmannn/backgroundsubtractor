#
# unet3 model
#
# This is an implementation of the paper
#
# "Image-to-Image Translation with Conditional Adversarial Networks"
# https://arxiv.org/abs/1611.07004
#
# Included are helper functions for convolution and transposed convolution.
#
# I have explicited opted for the 2*Xavier initialization scheme
# described in "Delving Deep into Rectifiers: Surpassing Human-Level Performance on ImageNet Classification"
# https://arxiv.org/pdf/1502.01852v1.pdf
#
# This is implemented in tensorflow as variance_scaling_initializer().
# One could also use the xavier initializer; I'm not sure it makes a big difference.
#
# D. Wingate
#


import numpy as np
import tensorflow as tf

#
# -----------------------------------------------------------------------
#
# Helper functions
#

#
# define a layer consisting of a convolution plus bias, followed by a relu and batch norm
#

def conv( x, filter_size=8, stride=2, num_filters=64, is_output=False, name="conv" ):

    filter_height, filter_width = filter_size, filter_size
    in_channels = x.get_shape().as_list()[-1]
    out_channels = num_filters

    with tf.variable_scope( name ):
        with tf.device('/cpu:0'):
            W = tf.get_variable( "W", shape=[filter_height, filter_width, in_channels, out_channels],
                                 initializer = tf.contrib.layers.variance_scaling_initializer() )
            b = tf.get_variable( "b", shape=[out_channels],
                                 initializer = tf.contrib.layers.variance_scaling_initializer() )

        conv = tf.nn.conv2d( x, W, [1, stride, stride, 1], padding="SAME" )
        out = tf.nn.bias_add(conv, b)
        if not is_output:
            out = tf.contrib.layers.batch_norm( tf.nn.relu(out) )

    return out

#
# a transpose convolution, plus bias, followed by relu
#

def convt( x, out_shape, filter_size=8, stride=2, is_output=False, name="convt" ):

    filter_height, filter_width = filter_size, filter_size
    in_channels = x.get_shape().as_list()[-1]

    with tf.variable_scope( name ):
        with tf.device('/cpu:0'):
            W = tf.get_variable( "W", shape=[filter_height, filter_width, out_shape[-1], in_channels],
                                 initializer = tf.contrib.layers.variance_scaling_initializer() )
            b = tf.get_variable( "b", shape=[out_shape[-1]],
                                 initializer = tf.contrib.layers.variance_scaling_initializer() )

        conv = tf.nn.conv2d_transpose( x, W, out_shape, [1, stride, stride, 1], padding="SAME" )
        out = tf.nn.bias_add( conv, b )
        if not is_output:
            out = tf.contrib.layers.batch_norm( tf.nn.relu(out) )

    return out

def upsample( x, channels_out, scale=2, filter_size=9, is_output=False, name="upsample", two_conv=True):
    batch, rows, cols, channels = x.shape
    with tf.variable_scope(name):
        up = tf.image.resize_images(x, np.array([rows*scale, cols*scale]), tf.image.ResizeMethod.NEAREST_NEIGHBOR)
        up = conv(up, filter_size, 1, channels_out, False, name="convA")

        if two_conv == True:
            up = conv(up, filter_size, 1, channels_out, is_output, name="convB")
    return up

#
# -----------------------------------------------------------------------
#
# The main model.
#
# The deep-u convnet has many layers and special concatenation
# operations that implement a sort of skip-layer.  The advantage of
# this architecture is that coarse, large-scale information can be
# blended with local information; this seems like a good match for the
# mix of global and local information in the tissue classification
# problem.
#

def unet( input_data, name="", output_channels=1):
    conv1a = conv( input_data, filter_size=3, stride=1, num_filters=64, name=name + "_conv1a" )
    conv1b = conv( conv1a, filter_size=3, stride=1, num_filters=64, name=name + "_conv1b" )
    pool12 = tf.nn.max_pool( conv1b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool12" )

    conv2a = conv( pool12, filter_size=3, stride=1, num_filters=128, name=name + "_conv2a" )
    conv2b = conv( conv2a, filter_size=3, stride=1, num_filters=128, name=name + "_conv2b" )
    pool23 = tf.nn.max_pool( conv2b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool23" )

    conv3a = conv( pool23, filter_size=3, stride=1, num_filters=256, name=name + "_conv3a" )
    conv3b = conv( conv3a, filter_size=3, stride=1, num_filters=256, name=name + "_conv3b" )
    pool34 = tf.nn.max_pool( conv3b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool34" )

    conv4a = conv( pool34, filter_size=3, stride=1, num_filters=512, name=name + "_conv4a" )
    conv4b = conv( conv4a, filter_size=3, stride=1, num_filters=512, name=name + "_conv4b" )
    pool45 = tf.nn.max_pool( conv4b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool45" )

    # -- this is the bottom of the U

    conv5a = conv( pool45, filter_size=3, stride=1, num_filters=1024, name=name + "_conv5a" )
    conv5b = conv( conv5a, filter_size=3, stride=1, num_filters=1024, name=name + "_conv5b" )

    # -- now work our way back up

    pool54 = upsample(conv5b, 512, filter_size=3, name="upa")
    pool54c = tf.concat( [pool54, conv4b], 3 )

    conv4ci = conv( pool54c, filter_size=3, stride=1, num_filters=512, name=name + "_conv4ci" )
    conv4di = conv( conv4ci, filter_size=3, stride=1, num_filters=512, name=name + "_conv4di" )

    pool43 = upsample(conv4di, 256, filter_size=3, name="upb")
    pool43c = tf.concat( [pool43, conv3b], 3 )

    conv3ci = conv( pool43c, filter_size=3, stride=1, num_filters=256, name=name + "_conv3ci" )
    conv3di = conv( conv3ci, filter_size=3, stride=1, num_filters=256, name=name + "_conv3di" )

    pool32 = upsample(conv3di, 128, filter_size=3, name="upc")
    pool32c = tf.concat( [pool32, conv2b], 3 )

    conv2ci = conv( pool32c, filter_size=3, stride=1, num_filters=128, name=name + "_conv2ci" )
    conv2di = conv( conv2ci, filter_size=3, stride=1, num_filters=128, name=name + "_conv2di" )

    pool21 = upsample(conv2di, 64, filter_size=3, name="upd")
    pool21c = tf.concat( [pool21, conv1b], 3 )

    conv1ci = conv( pool21c, filter_size=3, stride=1, num_filters=64, name=name + "_conv1ci" )
    conv1di = conv( conv1ci, filter_size=3, stride=1, num_filters=64, name=name + "_conv1di" )

    logits = conv( conv1di, filter_size=1, stride=1, num_filters=output_channels, is_output=True, name=name + "_logits" )

    return logits

def encode_decode( input_data, name="", output_channels=1 ):
    conv1a = conv( input_data, filter_size=9, stride=1, num_filters=64, name=name + "_conv1a" )
    conv1b = conv( conv1a, filter_size=9, stride=1, num_filters=64, name=name + "_conv1b" )
    pool12 = tf.nn.max_pool( conv1b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool12" )

    conv2a = conv( pool12, filter_size=5, stride=1, num_filters=128, name=name + "_conv2a" )
    conv2b = conv( conv2a, filter_size=5, stride=1, num_filters=128, name=name + "_conv2b" )
    pool23 = tf.nn.max_pool( conv2b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool23" )

    conv3a = conv( pool23, filter_size=3, stride=1, num_filters=256, name=name + "_conv3a" )
    conv3b = conv( conv3a, filter_size=3, stride=1, num_filters=256, name=name + "_conv3b" )
    pool34 = tf.nn.max_pool( conv3b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool34" )

    conv4a = conv( pool34, filter_size=3, stride=1, num_filters=512, name=name + "_conv4a" )
    conv4b = conv( conv4a, filter_size=3, stride=1, num_filters=512, name=name + "_conv4b" )
    pool45 = tf.nn.max_pool( conv4b, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool45" )

    # -- this is the bottom of the U

    conv5a = conv( pool45, filter_size=3, stride=1, num_filters=1024, name=name + "_conv5a" )
    conv5b = conv( conv5a, filter_size=3, stride=1, num_filters=1024, name=name + "_conv5b" )

    # -- now work our way back up

    pool54 = upsample(conv5b, 512, filter_size=3, name="upa")
    pool43 = upsample(pool54, 256, filter_size=3, name="upb")
    pool32 = upsample(pool43, 128, filter_size=5, name="upc")
    pool21 = upsample(pool32, 64, filter_size=9, name="upd")
    logits = conv(pool21, filter_size=1, stride=1, num_filters=output_channels, is_output=True, name=name + "_logits")

    return logits

def unet_small( input_data, name="", output_channels=1):
    conv1 = conv(input_data, filter_size=5, stride=1, num_filters=64, name=name + "_conv1")
    pool1 = tf.nn.max_pool(conv1, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool1")

    conv2 = conv(pool1, filter_size=3, stride=1, num_filters=128, name=name + "_conv2")
    pool2 = tf.nn.max_pool(conv2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool2")

    conv3 = conv(pool2, filter_size=3, stride=1, num_filters=256, name=name + "_conv3")
    pool3 = tf.nn.max_pool(conv3, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name=name + "_pool3")

    # -- this is the bottom of the U

    conv4 = conv(pool3, filter_size=3, stride=1, num_filters=512, name=name + "_conv5")

    # -- now work our way back up

    pool4 = upsample(conv4, 256, filter_size=3, name="up1", two_conv=False)
    skip1 = tf.concat([pool4, conv3], 3)
    conv5 = conv(skip1, filter_size=3, stride=1, num_filters=256, name=name + "_skip_conv1")

    pool5 = upsample(conv5, 128, filter_size=3, name="up2", two_conv=False)
    skip2 = tf.concat([pool5, conv2], 3)
    conv6 = conv(skip2, filter_size=3, stride=1, num_filters=128, name=name + "_skip_conv2")

    pool6 = upsample(conv6, 64, filter_size=5, name="up3", two_conv=False)
    skip3 = tf.concat([pool6, conv1], 3)
    conv7 = conv(skip3, filter_size=5, stride=1, num_filters=64, name=name + "_skip_conv3")

    logits = conv(conv7, filter_size=5, stride=1, num_filters=output_channels, is_output=True, name=name + "_logits" )

    return logits
