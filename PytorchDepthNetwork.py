import torch
from Unet import Unet
from torch.autograd import Variable

import numpy as np

class DepthNetwork:
    def __init__(self, color_channels, depth_channels_in, depth_channels_out, state=None):
        # assemble the network
        self.rgb_unet = Unet(color_channels, depth_channels_out)
        self.rgb_unet.cuda()
        self.depth_unet = Unet(depth_channels_in, depth_channels_out)
        self.depth_unet.cuda()

        # training ops
        self.rgb_optimizer = torch.optim.Adam(self.rgb_unet.parameters())
        self.depth_optimizer = torch.optim.Adam(self.depth_unet.parameters())
        self.reconstruction_loss = torch.nn.MSELoss()

        if state is not None:
            self.rgb_unet.load_state_dict(state['rgb_state_dict'])
            # self.depth_unet.load_state_dict(state['depth_state_dict'])
            self.rgb_optimizer = state['rgb_optimizer']
            # self.depth_optimizer = state['depth_optimizer']

    def get_state(self):
        return {'rgb_state_dict': self.rgb_unet.state_dict(),
                'depth_state_dict': self.depth_unet.state_dict(),
                'rgb_optimizer': self.rgb_optimizer,
                'depth_optimizer': self.depth_optimizer}

    def train(self, images, depths=None, groundtruth=None, mask=None):
        out1, out2, loss = self.forward(images, depths, groundtruth, mask, False)
        if depths is None:
            self.rgb_optimizer.zero_grad()
            loss.backward()
            self.rgb_optimizer.step()
        else:
            self.depth_optimizer.zero_grad()
            loss.backward()
            self.depth_optimizer.step()
        return out1, out2, loss

    def forward(self, images, depths=None, groundtruth=None, mask=None, volatile=True):
        if type(images) is np.ndarray:
            images = Variable(torch.from_numpy(images), requires_grad=False, volatile=volatile).cuda()
        if type(depths) is np.ndarray:
            depths = Variable(torch.from_numpy(depths.astype(np.float32)), requires_grad=False, volatile=volatile).cuda()
        if type(groundtruth) is np.ndarray:
            groundtruth = Variable(torch.from_numpy(groundtruth.astype(np.float32)), requires_grad=False, volatile=volatile).cuda()
        if type(mask) is np.ndarray:
            mask = Variable(torch.from_numpy(mask.astype(np.float32)), requires_grad=False, volatile=volatile).cuda()

        out1 = self.rgb_unet(images)
        out2 = None
        loss = None

        if depths is not None:
            combined_depths = torch.cat((out1, depths, mask), dim=1)
            out2 = self.depth_unet(combined_depths)
        if groundtruth is not None:
            prediction = out1 if out2 is None else out2
            # do not penalize areas not included in the mask
            if mask is not None:
                prediction = prediction * mask
                groundtruth = groundtruth * mask
            loss = self.reconstruction_loss(prediction, groundtruth)

        return out1, out2, loss
