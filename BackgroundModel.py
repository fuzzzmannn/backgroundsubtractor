from scipy.signal import medfilt
import FileManager as fm
import numpy as np
import cv2
import sys
import os

from normalize_data import load_normalized_input
from normalize_data import load_normalized_roi

class BackgroundModel():
    def __init__(self, rows, cols, channels, depth=5):
        self.library = np.zeros((depth, rows, cols, channels))
        self.indexes = np.zeros((rows, cols)).astype(np.uint8)
        self.depth = depth

        self.prev_percent_motion = 0.0

    def update_library(self, image, mask):
        mask = np.squeeze(mask).astype(np.uint8)

        # push back all library pixels about to be updated
        rows, cols = np.where(mask == 0)
        self.library[1:, rows, cols] = self.library[:-1, rows, cols]

        # track the index of the oldest pixels
        self.indexes[rows, cols] += 1
        self.indexes = np.clip(self.indexes, 0, self.depth)

        # add new pixels to library
        self.library[0, rows, cols] = image[rows, cols]

    def get_lookback_depth(self, percent_motion):
        min_depth = 5
        lo_thresh = .02
        hi_thresh = .25
        if percent_motion >= hi_thresh: return min_depth
        if percent_motion <= lo_thresh: return self.depth
        # check degree of motion between the low and high thresholds
        scalar = (hi_thresh - percent_motion) / (hi_thresh - lo_thresh)
        return min_depth + int(scalar * (self.depth - min_depth))

    def smooth_percent_motion(self, percent_motion):
        if percent_motion == self.prev_percent_motion:
            return percent_motion
        scale = .2 if percent_motion < self.prev_percent_motion else .01
        return scale * percent_motion + (1.0 - scale) * self.prev_percent_motion

    def get_background(self, percent_motion=0.0, roi_mask=None):
        s_percent_motion = self.smooth_percent_motion(percent_motion)
        lookback_depth = self.get_lookback_depth(s_percent_motion)

        min_index = np.min(self.indexes)
        if min_index < lookback_depth: lookback_depth = min_index

        self.prev_percent_motion = percent_motion
        mean_image = np.mean(self.library[:lookback_depth], axis=0)
        if roi_mask is not None:
            roi_mask = np.concatenate([roi_mask, roi_mask, roi_mask], axis=-1)
            mean_image[roi_mask > 0] = self.library[0][roi_mask > 0]
        return mean_image

def get_roi(folder_path, roi_filename):
    filename = os.path.join(folder_path, roi_filename)
    start, end = open(filename, 'r').read().split()
    return int(start), int(end)

def close(image, kernel_size=5):
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    image = cv2.dilate(image, kernel, iterations=2)
    return cv2.erode(image, kernel, iterations=2)

def get_percent_motion(image, background, threshold=.003):
    sq_diff = np.sum(np.square(image - background), axis=-1)
    diff_mask = np.zeros_like(sq_diff)
    diff_mask[sq_diff > threshold] = 1.0

    return np.mean(diff_mask)

def get_diff_mask(image, median, threshold_scale=.04, median_size=3, close_size=5):
    sq_diff = np.sum(np.square(image - median), axis=-1)
    diff_mask = np.zeros_like(sq_diff)

    scaled_threshold = np.max(sq_diff) * threshold_scale
    threshold = np.clip(scaled_threshold, .001, .09)
    # print(scaled_threshold, "\t", threshold)

    diff_mask[sq_diff > threshold] = 1.0
    diff_mask = medfilt(diff_mask, median_size)
    return np.expand_dims(close(diff_mask.astype(np.uint8), close_size), axis=-1)

def run_model(folder):
    start, end = get_roi("./" + folder, "temporalROI.txt")

    # load video data
    images = None
    if os.path.isfile("./" + folder + "/input.npy"):
        images = np.load("./" + folder + "/input.npy")
    else:
        images = load_normalized_input(folder)
    roi_masks = load_normalized_roi(folder)
    # create the output directory
    if not os.path.exists("./" + folder + "/background"):
        os.makedirs("./" + folder + "/background")

    median = fm.med_stack(images[:start])
    background = np.ones_like(median)
    kernel = np.ones((5,5), np.uint8)

    model = BackgroundModel(240, 320, 3, 60)
    model.update_library(median, np.zeros_like(model.indexes))

    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(folder + '_output.avi', fourcc, 30.0, (320*2,240*2))

    for i in range(images.shape[0]):
        background = median if i < int(start/2.0) else background
        roi_mask = np.expand_dims(roi_masks[i], axis=-1).astype(np.float32)
        if i < start: roi_mask = roi_mask * 0.0

        mask = get_diff_mask(images[i], background)
        mask = np.expand_dims(cv2.dilate(mask, kernel, iterations=2), axis=-1).astype(np.float32)
        percent_motion = get_percent_motion(images[i], background)
        # print(i, "/", images.shape[0], percent_motion)

        masks = (np.hstack([mask, roi_mask]) * 255.0).astype(np.uint8)
        masks = np.concatenate([masks, masks, masks], axis=-1)

        mask[roi_mask == 1.0] = 0
        model.update_library(images[i], mask)
        background = model.get_background(percent_motion, roi_mask)
        frame = (np.hstack([images[i], background]) * 255.0).astype(np.uint8)
        frame = np.vstack([masks, frame])

        background_fname = str("./" + folder + "/background/bg%06d.png") % (i+1)
        cv2.imwrite(background_fname, (background * 255.0).astype(np.uint8))
        out.write(frame)
        cv2.imshow("stuff", frame)
        cv2.waitKey(1)
    out.release()

if __name__ == "__main__":
    folders = sys.argv[1:]

    for folder in folders:
        run_model(folder)
