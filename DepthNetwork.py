import tensorflow as tf
from unet3_model import unet3_inference as unet

class DepthNetwork:
    def __init__(self, rows_in, cols_in, color_channels_in, depth_channels_in,
                learning_rate=1e-3, model_filename=None):
        tf.reset_default_graph()
        self.sess = tf.InteractiveSession()

        # placeholder variables
        rgb_input_shape = [None, rows_in, cols_in, color_channels_in]
        depth_input_shape = [None, rows_in, cols_in, depth_channels_in]
        label_shape = [None, rows_in, cols_in, 1]

        self.input_rgb_data = tf.placeholder(tf.float32, rgb_input_shape)
        self.input_depth_data = tf.placeholder(tf.float32, depth_input_shape)
        self.labels = tf.placeholder(tf.float32, label_shape)

        # assemble the network
        with tf.name_scope( "model" ):
            self.rgb_depth_prediction = unet(self.input_rgb_data, rows_in, cols_in, "rgb")
            self.combined_depth = tf.concat([self.rgb_depth_prediction, self.input_depth_data], axis=-1)
            self.final_prediction = unet(self.combined_depth, rows_in, cols_in, "combined")

        with tf.name_scope( "cost_function" ):
            self.mask = tf.cast(self.labels > 0, tf.float32)
            self.rgb_depth_loss = tf.nn.l2_loss((self.labels - self.rgb_depth_prediction) * self.mask)
            self.combined_depth_loss = tf.nn.l2_loss((self.labels - self.final_prediction) * self.mask)

        self.optimizer = tf.train.AdamOptimizer(learning_rate)
        self.rgb_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "rgb")
        self.combined_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "combined")

        self.train_rgb_predictor = self.optimizer.minimize(self.rgb_depth_loss, var_list=self.rgb_vars)
        self.train_combined_predictor = self.optimizer.minimize(self.combined_depth_loss, var_list=self.combined_vars)

        # load old network
        self.saver = tf.train.Saver()
        if model_filename is not None:
            self.saver.restore(self.sess, model_filename)
        else:
            self.sess.run(tf.global_variables_initializer())

    def save_model(self, filename):
        self.saver.save(self.sess, filename)

    def evaluate_rgb(self, images, groundtruth=None):
        fdict = {self.input_rgb_data:images}
        out_vars = [self.rgb_depth_prediction]

        if groundtruth is not None:
            fdict[self.labels] = groundtruth
            out_vars += [self.rgb_depth_loss]

        return self.sess.run(out_vars, feed_dict=fdict)

    def evaluate_combined(self, images, depths, groundtruth=None):
        fdict = {self.input_rgb_data:images, self.input_depth_data:depths}
        out_vars = [self.final_prediction]

        if groundtruth is not None:
            fdict[self.labels] = groundtruth
            out_vars += [self.combined_depth_loss]

        return self.sess.run(out_vars, feed_dict=fdict)


    def train_rgb(self, images, groundtruth, report_loss=False):
        fdict = {self.input_rgb_data:images, self.labels:groundtruth}
        self.train_rgb_predictor.run(feed_dict=fdict)

        if report_loss == True:
            return self.rgb_depth_loss.eval(feed_dict=fdict)
        return None

    def train_combined(self, images, depths, groundtruth, report_loss=False):
        fdict = {self.input_rgb_data:images, self.input_depth_data:depths,
                self.labels:groundtruth}
        self.train_combined_predictor.run(feed_dict=fdict)

        if report_loss == True:
            return self.combined_depth_loss.eval(feed_dict=fdict)
        return None
