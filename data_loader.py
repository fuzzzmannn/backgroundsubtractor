import os
import sys
import cv2
import json
import time
import skimage

import numpy as np
import normalize_data as norm

def open_dataset(filename):
    with open(filename) as f:
        return json.load(f)

# index is the index into the folder list, not the actual photo index
def load(dataset, folder, index):
    index = index % len(dataset[folder]) # wrap the index
    index = dataset[folder][index]

    image_fname = str("./" + folder + "/input/in%06d.jpg") % index
    image = norm.normalize_image(cv2.imread(image_fname, 1))

    background_fname = str("./" + folder + "/background/bg%06d.png") % index
    background = norm.normalize_image(cv2.imread(background_fname, 1))

    mask_fname = str("./" + folder + "/masks/mk%06d.png") % index
    mask = cv2.imread(mask_fname, 0)
    if mask is not None:
        mask = norm.normalize_groundtruth(mask)

    depth_fname = str("./" + folder + "/depths/mk%06d.png") % index
    depth = cv2.imread(depth_fname, 1)
    if depth is not None:
        depth = norm.normalize_image(depth)[..., 0]

    gt_fname = str("./" + folder + "/groundtruth/gt%06d.png") % index
    gt = cv2.imread(gt_fname, 0)
    if gt is not None:
        gt = norm.normalize_groundtruth(gt)

    return image, background, mask, depth, gt

# copied from https://gist.github.com/hasnainv/49dc4a85933de6b979f8
def extract_patches(image, window_shape=(13,13, 3), stride=3):
    nr, nc, ncolor = image.shape
    patches = skimage.util.view_as_windows(image, window_shape, stride)
    nR, nC, t, H, W, C = patches.shape
    nWindow = nR * nC
    patches =  np.reshape(patches, (nWindow, H, W, C))
    patches = patches.astype(np.float32, copy=False)
    return patches

def get_patch_stack(image, background, mask, depth, gt):
    stack = np.dstack([image, background, mask, depth, gt])
    stack = np.pad(stack, ((16,16),(16,16),(0,0)), 'constant', constant_values=0)

    return extract_patches(stack, (32, 32, 9), 8)

def get_patch_tuple(dataset, index, shuffle=False):
    folders = dataset.keys()
    tuples = [load(dataset, folder, index) for folder in folders]
    patches = np.concatenate([get_patch_stack(*t) for t in tuples], axis=0)

    if shuffle == True: np.random.shuffle(patches)
    return patches[...,:3], patches[...,3:6], patches[..., -3], patches[..., -2], patches[..., -1]

def get_random_indexes(roi_length, roi_start, num_frames):
    indexes = np.arange(roi_length) + roi_start
    np.random.shuffle(indexes)
    return np.sort(indexes[:num_frames])

def get_roi(folder_path, roi_filename="temporalROI.txt"):
    filename = os.path.join(folder_path, roi_filename)
    start, end = open(filename, 'r').read().split()
    return int(start), int(end)

def generate_dataset(folders, training_size=150, test_size=20):
    train = {}
    test = {}

    for folder in folders:
        start, end = get_roi("./" + folder)
        length = end - start
        indexes = get_random_indexes(length, start, training_size + test_size)
        indexes = indexes.tolist()

        train[folder] = indexes[:training_size]
        test[folder] = indexes[training_size:]
    return train, test

def get_max_length(dataset):
    folders = dataset.keys()
    max_length = float("-inf")

    for folder in folders:
        max_length = max(max_length, len(dataset[folder]))
    return max_length

def shuffle_dataset(dataset):
    folders = dataset.keys()

    for folder in folders:
        np.random.shuffle(dataset[folder])

def sort_dataset(dataset):
    folders = dataset.keys()

    for folder in folders:
        dataset[folder] = np.sort(dataset[folder]).tolist()

if __name__ == "__main__":
    # with open(sys.argv[1]) as f:
    #     dataset = json.load(f)
    # folders = dataset.keys()
    #
    # for i in range(get_max_length(dataset)):
    #     i_patches, b_patches, g_patches = get_patch_tuple(dataset, i)
    #     print(i_patches.shape, b_patches.shape, g_patches.shape)

    train_name = sys.argv[1]
    test_name = sys.argv[2]
    folders = sys.argv[3:]

    train, test = generate_dataset(folders, -1, -1)

    with open(train_name, 'w') as f:
        json.dump(train, f)
    with open(test_name, 'w') as f:
        json.dump(test, f)
